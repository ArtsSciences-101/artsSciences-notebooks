{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Pagerank and networks"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Today, we will cover the pagerank algorithm, which was invented by one of the Google founders (Larry Page) to rank the importance of websites. Then, we will use the algorithm to rank the importance of nodes in networks and try to visualize some networks. The output of the algorithm is a probability vector whose entries indicate the \"importance\" of each website (relative to the others, since they are restricted to sum up to 1)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Pagerank algorithm"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The pagerank vector is *essentially* the invariant distribution of a Markov chain which characterizes the behavior of a user who clicks randomly on the links that appear on the websites they visit.\n",
    "\n",
    "Let's try to formalize the idea through an example. Suppose we have a set of 3 websites and suppose that\n",
    "* Website 1: has links to websites 2 and 3\n",
    "* Website 2: has a link to website 1\n",
    "* Website 3: has a link to websites 1 and 2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We are going to use the following steps to create our transition matrix:\n",
    "1. Construct a matrix $\\mathbf{A}$ where the $(i,j)$-th entry $\\mathbf{A}_{ij}$ is 1 if website $i$ has a link to page $j$ and $0$ otherwise. For example $\\mathbf{A}_{12} = 1$ if website 1 has a link to website 2.\n",
    "\n",
    "For the example above, \n",
    "* In the first column, $\\mathbf{A}$ will have ones in the second and third rows.  \n",
    "* In the second column, we will have a one in the first row.\n",
    "* In the third column, we will have ones in the first and second rows:\n",
    "\n",
    "$$\n",
    "\\mathbf{A} = \\begin{bmatrix}0 & 1 & 1 \\\\ 1 & 0 & 1 \\\\ 1 & 0 &  0 \\end{bmatrix}\n",
    "$$\n",
    "\n",
    "2. Find our Markov matrix $\\mathbf{M}$ by dividing each column by its column sum. That is, \n",
    "$$\n",
    "\\mathbf{M}_{ij} = \\frac{\\mathbf{A}_{ij}}{\\sum_{j=1}^n \\mathbf{A}_{ij}}.\n",
    "$$\n",
    "Another way of writing the same thing using matrix multiplication and inverses: Let $\\mathbf{D} = \\mathrm{diag}(\\sum_{j=1}^n \\mathbf{A}_{1j}, \\sum_{j=1}^n \\mathbf{A}_{2j}, \\, ... \\, , \\sum_{j=1}^n \\mathbf{A}_{nj})$. Then, $\\mathbf{M} = \\mathbf{A} \\mathbf{D}^{-1}$.\n",
    "\n",
    "To turn $\\mathbf{A}$ into a Markov transition matrix, we need to make the columns sum to one, so we divide each column by the sum of all the entries in the column:\n",
    "\n",
    "$$\n",
    "\\mathbf{M} = \\begin{bmatrix}0 & 1 & 0.5 \\\\ 0.5 & 0 & 0.5 \\\\ 0.5 & 0 &  0 \\end{bmatrix}\n",
    "$$\n",
    "\n",
    "To see how the matrix multiplication would work, for this example, $\\mathbf{D}$ has the sum of each column as the diagonal entries:\n",
    "$$\n",
    "\\mathbf{D} = \\begin{bmatrix}2 & 0 & 0 \\\\ 0 & 1 & 0 \\\\ 0 & 0 & 2 \\end{bmatrix}.\n",
    "$$\n",
    "A diagonal matrix is easy to take the inverse of, because we can just take the (multiplicative) reciprocal of each diagonal entry (check this by hand):\n",
    "$$\n",
    "\\mathbf{D}^{-1} = \\begin{bmatrix} \\frac{1}{2} & 0 & 0 \\\\ 0 & 1 & 0 \\\\ 0 & 0 & \\frac{1}{2} \\end{bmatrix}.\n",
    "$$\n",
    "Then when we multiply $\\mathbf{A} \\mathbf{D}^{-1}$, we get\n",
    "$$\n",
    "\\mathbf{A} \\mathbf{D}^{-1} = \\begin{bmatrix}0 & 1 & 1 \\\\ 1 & 0 & 1 \\\\ 1 & 0 &  0 \\end{bmatrix}\n",
    "\\begin{bmatrix} \\frac{1}{2} & 0 & 0 \\\\ 0 & 1 & 0 \\\\ 0 & 0 & \\frac{1}{2} \\end{bmatrix}\n",
    "= \\begin{bmatrix} 0 & 1 & \\frac{1}{2} \\\\ \\frac{1}{2} & 0 & \\frac{1}{2} \\\\ \\frac{1}{2} & 0 & 0 \\end{bmatrix} = \\mathbf{M}.\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Given $\\mathbf{M}$, it is tempting to define the pagerank vector as *the* vector $\\pi$ that satisfies $\\mathbf{M} \\pi = \\pi$. Why? *Under some conditions,* the invariant distribution gives us the proportion of times we spend in each state if we run the chain for a long time (we learned that in HW4). Therefore, if we find the invariant distribution and rank websites according to their probabilties, we will be ranking them according to how often we visit them."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Explain why the tentative definition of the pagerank algorithm is not satisfactory. Give an explicit example where the pagerank vector would not be unique, and state conditions under which it would be."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To avoid the issues you described in Exercise 1, Larry Page tweaked the Markov chain a little bit. The new transition matrix $P$ is a matrix whose entries are $\\mathbf{P}_{ij} = d \\mathbf{M}_{ij} + (1-d)/n$, with $0 < d < 1$ (Page used $d = 0.85$).\n",
    "\n",
    "For the example above, our tweaked transition matrix would be:\n",
    "$$\n",
    "\\mathbf{P}_{ij} = 0.85 \\mathbf{M}_{ij} + (1-0.85)/3 = \n",
    "0.85 \\begin{bmatrix}0 & 1 & 0.5 \\\\ 0.5 & 0 & 0.5 \\\\ 0.5 & 0 & 0 \\end{bmatrix} + \\frac{0.15}{3} =\n",
    "\\begin{bmatrix}0 & 0.85 & (0.85)0.5 \\\\ (0.85)0.5 & 0 & (0.85)0.5 \\\\ (0.85)0.5 & 0 & 0 \\end{bmatrix} + 0.05 =\n",
    "\\begin{bmatrix}0.05 & 0.9 & 0.475 \\\\ 0.475 & 0.05 & 0.475 \\\\ 0.475 & 0.05 & 0.05 \\end{bmatrix}\n",
    "$$\n",
    "Is $\\mathbf{P}$ a Markov matrix?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import numpy.linalg as npl\n",
    "# Start with A\n",
    "A = np.matrix([[0,1,1],[1,0,1],[1,0,0]])\n",
    "print(A)\n",
    "\n",
    "# Calculate D by adding up columns\n",
    "totals = np.sum(A,axis=0)\n",
    "print (totals)\n",
    "# Put totals in the diagonal of matrix D\n",
    "D = np.diagflat(totals)\n",
    "print(D)\n",
    "\n",
    "#Invert D\n",
    "Dinv = npl.inv(D)\n",
    "print(Dinv)\n",
    "\n",
    "#Multiply A and Dinv\n",
    "M = np.matmul(A, Dinv)\n",
    "\n",
    "#M = np.matrix([[0,1,0.5],[0.5,0,0.5],[0.5,0,0]])\n",
    "\n",
    "d = 0.85\n",
    "P = d*M + (1-d)/3\n",
    "print(M)\n",
    "print(P)\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise 2\n",
    "\n",
    "* Find $\\mathbf{P}$ for the example you found in Exercise 1.  \n",
    "* Find the limiting distribution for $\\mathbf{P}$. That is, find the *pagerank vector.* \n",
    "* Explain why $\\mathbf{P}$ avoids the issues you described in Exercise 1."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise 3\n",
    "Study the sensitivity of the algorithm to the choice of $d$ in both the given example and the example you found in Exercise 1. Do the limiting distributions change much? What are the effects of choosing $d$ near 0 as opposed to chosing $d$ near 1?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise 4"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The pagerank algorithm gives high importance to websites that receive links from \"important\" websites. Find a chain where there is a state with a high pagerank that has links on just a few very important websites (but it doesn't receive a lot of links overall)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Application: Florentine Families Graph"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We want to start small, with the \"Florentine families graph.\" This dataset has 15 nodes (influential Florentine families in 1430) and 20 edges (marriages connecting the families). Before we compute the pagerank vector, let's take a quick look at the data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import networkx as nx\n",
    "# This dataset is an example dataset in the networkx package\n",
    "G = nx.florentine_families_graph()\n",
    "print(nx.info(G))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The **degree** of a node is the number of edges that \"touch\" it. Therefore, on average, each family is connected to 2.6667 other families (through marriages)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see what the data looks like:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# draw a picture of the graph\n",
    "G_pos = nx.spring_layout(G)\n",
    "plt.rcParams[\"figure.figsize\"] = [7,7]\n",
    "nx.draw(G,G_pos,node_size=20,with_labels=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can plot a bar graph showing the degrees for each family:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# First pull out the number of edges connecting each family and draw a histogram \n",
    "myTupleList = G.degree()\n",
    "#print(myTupleList)\n",
    "myEdges = [x[1] for x in myTupleList] #this gets a list of the 2nd  element of each tuple\n",
    "myFamilies = [x[0] for x in myTupleList] #this gets a list of the 1st element of each tuple\n",
    "print(myEdges)\n",
    "print(myFamilies)\n",
    "\n",
    "x = np.arange(15) #numbers from 0 to 14\n",
    "plt.rcParams[\"figure.figsize\"] = [16,9] #make a bigger plot\n",
    "plt.bar(x, myEdges)\n",
    "plt.xticks(x, myFamilies, rotation=90) #rotates names for readability\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And we can plot another network graph where the size of the nodes are proportional to their degrees."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "G_pos = nx.spring_layout(G)\n",
    "plt.rcParams[\"figure.figsize\"] = [7,7]\n",
    "nx.draw(G,G_pos,node_size=[v * 20 for v in myEdges],with_labels=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also find the matrix $A$ in order to find the pagerank vector."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "edges = nx.adjacency_matrix(G) # This command gets the matrix A from the data\n",
    "Afamilies = np.asarray(edges.todense()) # convert it to an np array\n",
    "print(Afamilies)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise 5\n",
    "Given the matrix ``Afamilies``:\n",
    "\n",
    "* Find the limiting distribution for d = 0.85.\n",
    "* Find a barplot whose columns are the pageranks for each family.\n",
    "* Plot the networks with node sizes proportional to their pageranks.\n",
    "\n",
    "[Hint: We *highly* recommend that you re-use code snippets above!]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Degrees and pagerank are not the only measures to quantify the importance of a node. Another notion is **\"betweenness centrality.\"**\n",
    "\n",
    "\n",
    "From Wikipedia:\n",
    "\n",
    "*\"Betweenness centrality is a measure of centrality in a graph based on shortest paths. For every pair of [nodes] in a [...] graph, there exists at least one shortest path between the [nodes] such that [...] the number of edges that the path passes through is minimized. The betweenness centrality for each [node] is the number of these shortest paths that pass through the [node].\"*\n",
    "\n",
    "Loosely, a node has a high \"between centrality\" if a lot of shortest paths between nodes go through it. \n",
    "\n",
    "Let's compute the betweenness centrality of the nodes of the graph and plot the results."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "between = nx.betweenness_centrality(G)\n",
    "myEdges2 = between.values()\n",
    "plt.bar(x, myEdges2)\n",
    "plt.xticks(x, myFamilies, rotation=90) #rotates names for readability\n",
    "plt.show()\n",
    "nx.draw(G,G_pos,node_size=[v * 2e3 for v in myEdges2],with_labels=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise 6"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Compare how the families are ranked according to their degree, pagerank, and betweenness centrality. Interpret the differences in terms of the definitions of the metrics and what you see in the graph."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Extra exercise 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Study the robustness of the pagerank vector to the choice of $d$. In particular, what happens if $d \\approx 0$ or $d \\approx 1$?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Extra application: Facebook friends data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is a network consisting of the Facebook friends of 10 people. Again, our goal is identifying the most influential people in the network. \n",
    "\n",
    "Sources:\n",
    "[1] https://snap.stanford.edu/data/egonets-Facebook.html\n",
    "[2] https://blog.dominodatalab.com/social-network-analysis-with-networkx/"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will use a new library (networkx) that is useful for analyzing and visualizing network data. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import networkx as nx\n",
    "\n",
    "\n",
    "# read in data, which is stored as a \"Graph\"\n",
    "G_fb = nx.read_edgelist(\"facebook_combined.txt\",create_using = nx.Graph(),nodetype = int)\n",
    "print(nx.info(G_fb))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this application, the degree of a node is the number of Facebook friends that someone has. So, on average, the users in our dataset have 43.6910 friends *within the network*. Recall that we only have the complete list of Facebook friends for 10 people. The distribution of degrees is heavily skewed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "myTupleList = G_fb.degree()\n",
    "myList = [x[1] for x in myTupleList]\n",
    "plt.hist(myList)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The code below orders the pairs (node, degree) in ascending order and prints it. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "myTuple = nx.degree(G_fb)\n",
    "sorted_by_second = sorted(myTuple, key=lambda tup: tup[1])\n",
    "print(sorted_by_second)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can find the top 10 nodes with the biggest degree with the following command:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(sorted_by_second[-10:])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And we can plot the network with node sizes proportional to their degrees as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "degsTuple = G_fb.degree()\n",
    "G_pos = nx.spring_layout(G_fb)\n",
    "nodeList = [x[0] for x in degsTuple]\n",
    "valueList = [x[1] for x in degsTuple]\n",
    "nx.draw(G_fb, G_pos, nodelist=nodeList, node_size=[v*0.1 for v in valueList])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can find $A$ to apply the pagerank algorithm to this network. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A_fb = nx.adjacency_matrix(G_fb) # finds the matrix A given a graph object\n",
    "A_fb = np.asarray(A_fb.todense()) # convert it to an np array"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Extra exercise 2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1. Find the pagerank vector and betweenness centralities for the Facebook network. \n",
    "2. Plot the networks with node sizes proportional to their pagerank and betweenness centralities.\n",
    "3. Find the top 10 nodes with the biggest pagerank and betweenness centralities.\n",
    "\n",
    "To sort the pagerank vector, you might find these commands useful. If your pagerank vector is called ``pagerank``:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(pagerank.argsort()[-10:]) # top 10, from 10 to 1 [most influential]\n",
    "print(pagerank)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can find the top 10 nodes with biggest betweenness centralities by slightly adapting the code for finding the top 10 nodes with highest degree."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
